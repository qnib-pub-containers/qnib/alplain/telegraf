# syntax=docker/dockerfile:1.4
FROM alpine:3.19 AS down
ARG TARGETARCH
WORKDIR /opt/
RUN wget -qO- https://dl.influxdata.com/telegraf/releases/telegraf-1.29.2_linux_${TARGETARCH}.tar.gz|tar xfz - --strip-components=2

FROM qnib/alplain-init:3.19.0
ENV INPUT_PORT=1234
ENV REMOTE_URL=http://influxdb:8086
ENV INFLUX_TOKEN=qnib
ENV INFLUX_ORG=qnib
ENV INFLUX_BUCKET=metrics
ENV INPUT_FORMAT=prometheusremotewrite
ENV DOCKER_GATHER_SERVICES=false
COPY --from=down /opt/usr/bin/telegraf /usr/bin/
COPY opt/qnib/telegraf/start.sh /opt/qnib/telegraf/start.sh
COPY opt/entry/20-telegraf.sh /opt/entry/
COPY opt/qnib/gonja/telegraf.conf.tmpl /opt/qnib/gonja/telegraf.conf.tmpl
CMD ["/opt/qnib/telegraf/start.sh"]